#include <iostream>

#include <libb/b.h>

int main() {
  std::cout << "Hello, World!" << std::endl;
  std::cout << getab() << std::endl;
  return 0;
}
