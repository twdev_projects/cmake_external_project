# ExternalProject super project example

This repo shows an example skeleton of super project using CMake's ExternalProject.

Read more on my [twdev.blog](https://twdev.blog/2024/02/cmake_external_project).
